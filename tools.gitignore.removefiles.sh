#!/usr/bin/env bash

git rm -r --cached .
git add .
git commit -m "Removing all files in .gitignore"
