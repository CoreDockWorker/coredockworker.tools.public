#!/usr/bin/env bash

# Définir la couleur jaune
yellow='\033[1;33m'
reset='\033[0m'

# Télécharger le script et le rendre exécutable
echo -e "${yellow}Téléchargement du script...${reset}"
curl -O https://gitlab.com/CoreDockWorker/coredockworker.tools.public/raw/master/tools.gitpush.simple.sh
echo -e "${yellow}Rendre le script exécutable...${reset}"
chmod +x tools.gitpush.simple.sh

# This directory path
DIR="$(cd "$(dirname "$0")" && pwd -P)"
# Full path of this script
THIS="${DIR}/$(basename "$0")"

# Fonction pour vérifier et définir une valeur de configuration Git
check_and_set_git_config() {
  local config_key=$1
  local config_value=$2

  # Définir la couleur verte
  green='\033[0;32m'
  reset='\033[0m'

  # Vérifie si la configuration est définie localement
  if git config --local "$config_key" >/dev/null 2>&1; then
    echo -e "${green}# La valeur '$config_key' est déjà définie localement${reset}"
    echo "$(git config --local --get "$config_key")"
  # Vérifie si la configuration est définie globalement
  elif git config --global "$config_key" >/dev/null 2>&1; then
    echo -e "${green}# La valeur '$config_key' est déjà définie globalement${reset}"
    echo "$(git config --global --get "$config_key")"
  # Si la configuration n'est pas définie localement ou globalement, la définir globalement
  else
    git config --global "$config_key" "$config_value"
    echo -e "${green}# La valeur '$config_key' a été définie globalement${reset}"
    echo "$(git config --global --get "$config_key")"
  fi
}

# Définir la valeur de pull.rebase
check_and_set_git_config "pull.rebase" "true"
check_and_set_git_config "user.email" $(whoami)@$HOSTNAME
check_and_set_git_config "user.name" $(whoami)@$HOSTNAME

echo "**Les 3 valeurs Git indispensables ont été définies.**"


# Définir les variables de chemin
echo -e "${yellow}Définition des variables de chemin...${reset}"
DIR="$(cd "$(dirname "$0")" && pwd -P)"
THIS="${DIR}/$(basename "$0")"

echo -e "${yellow}Vérification SSH...${reset}"
set -x
eval "$(ssh-agent -s)"
ssh-add ~/.ssh/id_rsa
ssh -T git@gitlab.com
git remote -v
set +x

# Exécuter les commandes Git
echo -e "${yellow}Récupération des dernières modifications...${reset}"
set -x
git fetch --all
set +x

echo -e "${yellow}Fusion des changements distants...${reset}"
set -x
git pull origin $(git rev-parse --abbrev-ref HEAD)
set +x

echo -e "${yellow}Ajout de tous les fichiers modifiés...${reset}"
set -x
git add --all
set +x

echo -e "${yellow}Création du commit...${reset}"
set -x
git commit --author="$(whoami) <$(whoami)@$HOSTNAME>" -am "$(whoami)@$HOSTNAME $(TZ="Europe/Paris" date +'%Y-%m-%d %H:%M %Z %A')"
set +x

echo -e "${yellow}Envoi des modifications vers le dépôt distant...${reset}"
set -x
git push origin $(git rev-parse --abbrev-ref HEAD)
set +x

echo -e "${yellow}Récupération finale des changements distants...${reset}"
set -x
git pull origin $(git rev-parse --abbrev-ref HEAD)
set +x
